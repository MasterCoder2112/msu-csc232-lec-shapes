# Shapes Hierarchy Lecture

This repo contains files related to the Shapes hierarchy discussed in class.

## Compiling and Executing `Main.cpp`

NOTE: The output shown below is based upon the code present in revision 1efb268.

### Using `cmake`

This repo contains a `CMakeLists.txt` file that cmake can use to generate Makefiles. A fairly ubquitous generator would be "Unix Makefiles." Navigate into build directory and pick your generator type. For example to build Unix Makefiles, follow these steps:

```
$ cd build/unix
$ cmake -G "Unix Makefiles" ../..
... lots of output ...
$ make clean && make
[ 16%] Building CXX object CMakeFiles/ShapesDemo_Test.dir/src/debug/UnitTestRunner.cpp.o
[ 33%] Linking CXX executable ShapesDemo_Test
[ 33%] Built target ShapesDemo_Test
[ 50%] Building CXX object CMakeFiles/ShapesDemo.dir/src/release/Main.cpp.o
[ 66%] Building CXX object CMakeFiles/ShapesDemo.dir/src/release/Rectangle.cpp.o
[ 83%] Building CXX object CMakeFiles/ShapesDemo.dir/src/release/Square.cpp.o
[100%] Linking CXX executable ShapesDemo
[100%] Built target ShapesDemo
$ ./ShapeDemo
name:      Rectangle
length:         4.00
width:          2.00
perimeter:     12.00
area:           8.00

name:      Rectangle
length:         8.00
width:          4.00
perimeter:     24.00
area:          32.00

name:         Square
length:         3.00
width:          3.00
perimeter:     12.00
area:           9.00

name:      Rectangle
length:         2.50
width:          2.50
perimeter:     10.00
area:           6.25
$
```

### Using `g++`

To compile and execute this manually using `g++`, follow these steps:

```
$ cd src/release
$ g++ -std=c++14 -Wall *.cpp -o ShapesDemo
$ ./ShapeDemo
name:      Rectangle
length:         4.00
width:          2.00
perimeter:     12.00
area:           8.00

name:      Rectangle
length:         8.00
width:          4.00
perimeter:     24.00
area:          32.00

name:         Square
length:         3.00
width:          3.00
perimeter:     12.00
area:           9.00

name:      Rectangle
length:         2.50
width:          2.50
perimeter:     10.00
area:           6.25
$
```

Type the following in your terminal window to see which `cmake` generators are available on your system:

```
$ cmake --help
```

## Revision History

* 1efb268 - No pointers, just object references. This revision demonstrates the affect of static binding.
* f40be49 - Use pointers to show the affect of dynamic binding. 
